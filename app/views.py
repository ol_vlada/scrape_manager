from flask import Response


from app import appl
from utils.connect_to_redis import redis_connect

r = redis_connect()
@appl.route('/')
def home():
    return 'To start spider, send get request on http://35.166.121.168/start_spider/spider_name'

@appl.route('/start_spider/<spider_name>')
def start_spider(spider_name):
    r.lpush('%s:start_urls' % (spider_name), spider_name)
    response = Response(
        response='Task added to redis queue. Please wait to start spider',
        status=200,
        mimetype='text/xml; charset=utf-8'
    )
    return response



import os

REDIS_HOST = '127.0.0.1'
REDIS_PORT = '6379'

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
firebase_settings_file = os.path.join(BASE_DIR, 'scrapymanager-5e83a-firebase-adminsdk-rcvi1-0b8dc50b63.json')
project_name = 'scrapymanager-5e83a'
spider_status = {
    'idle': 'idle',
    'panding': 'panding'
}

parsing_status = {
    'success': 'success',
    'failed': 'failed',
}

LOG_FILE_PATH = '/logs'

try:
    from local_config import *
except ImportError:
    pass
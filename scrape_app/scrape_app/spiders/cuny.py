# -*- coding: utf-8 -*-

import scrapy
from datetime import datetime
from logging import root as logger
from scrape_app.scrape_app.base_spider import BaseSpider


class CunyEduSpider(BaseSpider):
    name = "cuny"
    domain = "events.cuny.edu"

    @staticmethod
    def get_start_data():
        start_data = {
            'url': 'http://events.cuny.edu/'
        }
        return start_data

    def parse(self, response):
        events_list = self.get_event(response)
        if events_list:
            for event in events_list:
                if self.domain in event:
                    yield scrapy.Request(url=event, callback=self.parse_events_page, dont_filter=True)

        next_page_link = response.xpath('//div[@class="pagination"]/a[contains(text(), "next")]/@href').extract()
        if next_page_link:
            yield from self.parse_next_page_link(next_page_link, self.parse)

    def get_event(self, response):
        events_list = response.xpath('//div[@class="wpb_wrapper"]/ul/li/div/h2/a/@href').extract()
        if events_list:
            return events_list

    @staticmethod
    def get_title(response):
        return response.xpath('//h1[@class="cec-h1_detailtitle"]/text()').extract_first('').strip()

    @staticmethod
    def get_brief_description(response):
        return None

    @staticmethod
    def get_description(response):
        description_data = response.xpath('//div[contains(@class, "wpb_cuny_text_box")]/'
                                          'div[@class="wpb_wrapper"]/div//text()').extract()
        description = ','.join([x.replace('\r', '').replace('\n', '').strip() for x in description_data])
        if description:
            return description

    @staticmethod
    def get_date_from(response):
        start_date_time = response.xpath('//h4[contains(text(), "Date")]/following-sibling::p/text()').extract_first('').split('—')[0].strip()
        if start_date_time:
            try:
                formatted_date = datetime.strptime(start_date_time, '%B %d, %Y').strftime('%d/%m/%Y')
                return formatted_date
            except Exception as e:
                logger.warning(e)
                return None

    @staticmethod
    def get_date_to(response):
        date_to = response.xpath('//h4[contains(text(), "Date")]/'
                                 'following-sibling::p/text()').extract_first('').split('—')[-1].strip()
        if date_to:
            try:
                formatted_date = datetime.strptime(date_to, '%B %d, %Y').strftime('%d/%m/%Y')
                return formatted_date
            except Exception as e:
                logger.warning(e)
                return None

    @staticmethod
    def get_start_time(response):
        start_date_time = response.xpath('//h4[contains(text(), "Time")]/following-sibling::p/'
                                         'text()').extract_first('').split('—')[0].strip()
        if start_date_time:
            return start_date_time

    @staticmethod
    def get_end_time(response):
        end_date_time = response.xpath('//h4[contains(text(), "Time")]/following-sibling::p/'
                                         'text()').extract_first('').split('—')[-1].strip()
        if end_date_time:
            return end_date_time

    @staticmethod
    def get_event_type(response):
        return None

    @staticmethod
    def get_event_website(response):
        return response.url

    @staticmethod
    def get_ticket_url(response):
        register = response.xpath('//a[contains(text(), "Register")]/@href | '
                                  '//p[contains(text(), "Register")]/a/@href').extract_first()

        return register if register else response.url

    @staticmethod
    def get_speakers(response):
        return None

    @staticmethod
    def get_in_group_id(response):
        return '7043'

    @staticmethod
    def get_organization_name(response):
        return "CUNY"

    @staticmethod
    def get_photo_url(response):
        return response.xpath('//div[@class="event-img"]/img/@src').extract_first('').strip()

    @staticmethod
    def get_location(response):
        location_data = response.xpath('//h4[contains(text(), "Building")]'
                                       '/following-sibling::p/text()').extract_first('').strip()
        return location_data

    @staticmethod
    def get_room(response):
        room = response.xpath('//h4[contains(text(), "Room")]/following-sibling::p/text()').extract_first('').strip()
        return room

    @staticmethod
    def get_address(response):
        zip = CunyEduSpider.get_zip(response)
        city = CunyEduSpider.get_city(response)
        state = CunyEduSpider.get_state(response)
        room = CunyEduSpider.get_room(response)
        location = CunyEduSpider.get_location(response)
        address = response.xpath('//h4[contains(text(), "Address")]/following-sibling::p/'
                                 'text()').extract_first('').strip()

        if address:
            address = address.replace(zip, '').replace('Room', '').replace(room, '').\
                replace(city, '').replace(state, '').replace(location, '')
        return address

    @staticmethod
    def get_city(response):
        return 'New York'

    @staticmethod
    def get_state(response):
        return 'NY'

    @staticmethod
    def get_zip(response):
        return '10451'

    @staticmethod
    def get_keywords(response):
        theme = response.xpath('//dd[@class="filter-theme"]/p/a/text()').extract()
        if theme:
            return theme

    @staticmethod
    def get_hashtag(response):
        hashtags = response.xpath('//dd[@class="event-hashtag"]/p/a/@href').extract()
        if hashtags:
            return hashtags

    @staticmethod
    def get_contact_name(response):
        return None

    @staticmethod
    def get_contact_phone(response):
        phone = response.xpath('//h4[contains(text(), "Phone")]/following-sibling::p/'
                               'text()').extract_first('').strip()
        return phone

    @staticmethod
    def get_contact_email(response):
        return response.xpath('//p[contains(text(), "Email")]/a/text()').extract_first()

    @staticmethod
    def get_ticket_info(response):
        cost = response.xpath('//h4[contains(text(), "Admission")]/following-sibling::p/text()').extract_first('').strip()
        ticket_info = dict(ticket_price=cost,
                           ticket_name='',
                           description='')
        return ticket_info

    @staticmethod
    def get_speaker_info(response):
        return None

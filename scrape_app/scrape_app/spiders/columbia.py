# -*- coding: utf-8 -*-

import json
import re
from json import JSONDecodeError
from datetime import datetime
from scrape_app.scrape_app.base_spider import BaseSpider


class ColumbiaEduSpider(BaseSpider):
    name = "columbia"

    @staticmethod
    def get_start_data():
        start_data = {
            'url': 'https://events.columbia.edu/feeder/main/eventsFeed.do?f=y&sort=dtstart.utc:asc&fexpr='
                   '(((vpath=%22/public/aliases/Type/Panel%22)%20or%20(vpath=%22/public/aliases/Type/Conference%22)'
                   '%20or%20(vpath=%22/public/aliases/Type/Lecture%22)%20or%20(vpath=%22/public/aliases/Type/Film%22))'
                   '%20and%20((vpath=%22/public/aliases/Audience/Public%22)))%20and%20(categories.href=%22/'
                   'public/.bedework/categories/org/UniversityEvents%22)%20and%20'
                   '(entity_type=%22event%22%7Centity_type=%22todo%22)&skinName=list-json&count=200&days=31'
        }
        return start_data

    def parse(self, response):
        try:
            json_data = json.loads(response.body_as_unicode())
        except JSONDecodeError:
            raise JSONDecodeError('Response obj can not be converted to json')
        if json_data:
            events = self.get_event(json_data)
            if events:
                for event in events:
                    yield from self.parse_events_page(event)

    def get_event(self, json_data):
        return json_data.get('bwEventList', {}).get('events')

    @staticmethod
    def get_title(response):
        return response.get('summary')

    @staticmethod
    def get_brief_description(response):
        return None

    @staticmethod
    def get_description(response):
        description = response.get('description').strip().replace('\n', '')
        if description:
            return description

    @staticmethod
    def get_date_from(response):
        date_from = response.get('start', {}).get('shortdate')
        if date_from:
            dt = datetime.strptime(date_from, "%m/%d/%y")
            return datetime.strftime(dt, "%m/%d/%Y")

    @staticmethod
    def get_date_to(response):
        date_to = response.get('end', {}).get('shortdate')
        if date_to:
            dt = datetime.strptime(date_to, "%m/%d/%y")
            return datetime.strftime(dt, "%m/%d/%Y")

    @staticmethod
    def get_start_time(response):
        return response.get('start', {}).get('time')

    @staticmethod
    def get_end_time(response):
        return response.get('end', {}).get('time')

    @staticmethod
    def get_event_type(response):
        event_type = []
        for prop in response.get('xproperties'):
            if 'X-BEDEWORK-ALIAS' in prop.keys():
                type = prop.get('X-BEDEWORK-ALIAS', {}).get('values', {}).get('text')
                split_type = type.split('/')
                if split_type[-2] == 'Type':
                    event_type.append(split_type[-1])
        return event_type

    @staticmethod
    def get_event_website(response):
        return response.get('eventlink')

    @staticmethod
    def get_ticket_url(response):
        return response.get('eventlink')

    @staticmethod
    def get_speakers(response):
        return None

    @staticmethod
    def get_in_group_id(response):
        return '7181'

    @staticmethod
    def get_organization_name(response):
        return 'Columbia University'

    @staticmethod
    def get_photo_url(response):
        for prop in response.get('xproperties'):
            if 'X-BEDEWORK-IMAGE' in prop.keys():
                image = prop.get('X-BEDEWORK-IMAGE', {}).get('values', {}).get('text')
                return image if 'http' in image else 'https://events.columbia.edu/pubcaldav'+ image

    @staticmethod
    def get_location(response):
        return response.get('location', {}).get('address').split(',')[0]

    @staticmethod
    def get_room(response):
        address = response.get('location', {}).get('address')
        room = re.findall(r'Room(.*)', address)
        if room:
            return room[0].strip()

    @staticmethod
    def get_address(response):
        location = ColumbiaEduSpider.get_location(response)
        room = ColumbiaEduSpider.get_room(response)
        state = ColumbiaEduSpider.get_state(response)
        zip = ColumbiaEduSpider.get_zip(response)
        address = response.get('location', {}).get('address')
        if address:
            if location:
                address = address.replace(location, '')
            if room:
                address = address.replace('Room', '').replace(room, '')
            if state:
                address = address.replace(state, '')
            if zip:
                address = address.replace(zip, '')
            return address.strip()

    @staticmethod
    def get_city(response):
        return response.get('start', {}).get('timezone')

    @staticmethod
    def get_state(response):
        return 'NY'

    @staticmethod
    def get_zip(response):
        return '10027'

    @staticmethod
    def get_keywords(response):
        event_type = []
        for prop in response.get('xproperties'):
            if 'X-BEDEWORK-ALIAS' in prop.keys():
                type = prop.get('X-BEDEWORK-ALIAS', {}).get('values', {}).get('text')
                split_type = type.split('/')
                if split_type[-2] == 'Category':
                    event_type.append(split_type[-1])
        return event_type

    @staticmethod
    def get_hashtag(response):
        return None

    @staticmethod
    def get_contact_name(response):
        return response.get('contact', {}).get('name')

    @staticmethod
    def get_contact_phone(response):
        return response.get('contact', {}).get('phone')

    @staticmethod
    def get_contact_email(response):
        return response.get('contact', {}).get('email')

    @staticmethod
    def get_ticket_info(response):
        ticket_info = dict(ticket_price=response.get('cost'),
                           ticket_name='',
                           description='')
        return ticket_info

    @staticmethod
    def get_speaker_info(response):
        return None
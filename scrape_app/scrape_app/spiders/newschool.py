# -*- coding: utf-8 -*-

import scrapy
from datetime import datetime
from scrape_app.scrape_app.base_spider import BaseSpider


class NewschoolEduSpider(BaseSpider):
    name = "newschool"
    exclude_event_list = ['Exhibitions and Openings']

    @staticmethod
    def get_start_data():
        start_data = {
            'url': 'https://events.newschool.edu/calendar/month/' +
                  str(datetime.today().year) + '/' + str(datetime.today().month),
            'headers': {
                'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'Connection': 'keep-alive',
                'Host': 'events.newschool.edu',
            }
        }
        return start_data

    def parse(self, response):
        events_list = self.get_event(response)
        if events_list:
            for event in events_list:
                yield scrapy.Request(url=event, dont_filter=True, callback=self.parse_events_page)

    def get_event(self, response):
        events_list = response.xpath('//div[@id="event_results"]/'
                                     'div[contains(@class, "event_item")]/'
                                     'div[@class="item_content_medium"]/'
                                     'div[@class="heading"]/h3/a/@href').extract()
        if events_list:
            return events_list

    @staticmethod
    def get_title(response):
        return response.xpath('//h1[@class="summary"]/span/text()').extract_first('').strip()

    @staticmethod
    def get_brief_description(response):
        return None

    @staticmethod
    def get_description(response):
        description_data = response.xpath(
            '//div[@class="description"]//text() | //p[@class="description"]//text()').extract()
        description = ','.join([x.replace('\r', '').replace('\n', '').strip() for x in description_data])
        if description:
            return description

    @staticmethod
    def get_date_from(response):
        start_date_time = response.xpath('//abbr[@class="dtstart"]/'
                                         '@title').extract_first('').rsplit('-', 1)[0]
        if start_date_time:
            return datetime.strptime(start_date_time, '%Y-%m-%dT%H:%M:%S').strftime('%m/%d/%Y')

    @staticmethod
    def get_date_to(response):
        end_date_time = response.xpath(
            '//abbr[@class="dtend"]/@title').extract_first('').rsplit('-', 1)[0]
        if end_date_time:
            return datetime.strptime(
                end_date_time, '%Y-%m-%dT%H:%M:%S').strftime('%m/%d/%Y')

    @staticmethod
    def get_start_time(response):
        start_date_time = response.xpath('//abbr[@class="dtstart"]/'
                                         '@title').extract_first('').rsplit('-', 1)[0]
        if start_date_time:
            return datetime.strptime(start_date_time, '%Y-%m-%dT%H:%M:%S').strftime('%H:%M %p')

    @staticmethod
    def get_end_time(response):
        end_date_time = response.xpath(
            '//abbr[@class="dtend"]/@title').extract_first('').rsplit('-', 1)[0]
        if end_date_time:
            return datetime.strptime(
                    end_date_time, '%Y-%m-%dT%H:%M:%S').strftime('%H:%M %p')

    @staticmethod
    def get_event_type(response):
        return response.xpath('//dd[@class="filter-event_types"]/p/a/text()').extract()

    @staticmethod
    def get_event_website(response):
        return response.url

    @staticmethod
    def get_ticket_url(response):
        register_url = response.xpath('//div[@class="ticket_action"]/a/@href').extract_first()
        event_website = response.xpath('//dd[@class="event-website"]/p/a/@href').extract_first()
        return register_url if register_url else event_website

    @staticmethod
    def get_speakers(response):
        speakers = response.xpath('//p[contains(text(), "Speakers")]/following-sibling::ul/li/b//text()').extract()
        if speakers:
            return speakers

    @staticmethod
    def get_in_group_id(response):
        return '7028'

    @staticmethod
    def get_organization_name(response):
        return None

    @staticmethod
    def get_photo_url(response):
        return response.xpath('//div[@class="box_image grid_4"]//img/@src').extract_first('')

    @staticmethod
    def get_location(response):
        location_data = response.xpath('//h3[@class="location"]/a/text()').extract()
        return ' '.join([x.strip() for x in location_data])

    @staticmethod
    def get_room(response):
        return None

    @staticmethod
    def get_address(response):
        location_data = response.xpath('//h3[@class="location"]/small[@class="street-address"]/text()').extract()
        if location_data:
            return ' '.join(location_data)

    @staticmethod
    def get_city(response):
        return 'New York'

    @staticmethod
    def get_state(response):
        return 'NY'

    @staticmethod
    def get_zip(response):
        return '10011'

    @staticmethod
    def get_keywords(response):
        theme = response.xpath('//dd[@class="filter-theme"]/p/a/text()').extract()
        topic = response.xpath('//dd[@class="filter-topic"]/p/a/text()').extract()
        return theme if theme else topic

    @staticmethod
    def get_hashtag(response):
        hashtags = response.xpath('//dd[@class="event-hashtag"]/p/a/text()').extract()
        if hashtags:
            return hashtags

    @staticmethod
    def get_contact_name(response):
        return None

    @staticmethod
    def get_contact_phone(response):
        phone = response.xpath('//p[contains(text(), "Phone")]/text()').extract_first('').replace('Phone: ', '')
        if phone:
            return phone

    @staticmethod
    def get_contact_email(response):
        return response.xpath('//p[@class="description"][contains(text(), "Email")]/a/text() |'
                              ' //p[contains(text(), "Email")]/a/text()').extract_first()

    @staticmethod
    def get_ticket_info(response):
        cost = response.xpath('//dd[@class="event-cost"]/p/text()').extract_first('')
        ticket_info = dict(ticket_price=cost,
                           ticket_name='',
                           description='')
        return ticket_info

    @staticmethod
    def get_speaker_info(response):
        return None
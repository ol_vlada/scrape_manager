# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ScrapeAppItem(scrapy.Item):
    title = scrapy.Field()  # required
    brief_description = scrapy.Field()
    description = scrapy.Field()
    date_from = scrapy.Field()  # required
    date_to = scrapy.Field()
    start_time = scrapy.Field()  # required
    end_time = scrapy.Field()
    event_type = scrapy.Field()
    event_website = scrapy.Field()  # required
    ticket_url = scrapy.Field()
    speakers = scrapy.Field()  # list of speakers
    in_group_id = scrapy.Field()  # required
    organization_name = scrapy.Field()  # required
    photo_url = scrapy.Field()
    location = scrapy.Field()
    room = scrapy.Field()
    address = scrapy.Field()
    city = scrapy.Field()
    state = scrapy.Field()
    zip = scrapy.Field()
    keywords = scrapy.Field()
    hashtag = scrapy.Field()
    contact_name = scrapy.Field()
    contact_phone = scrapy.Field()
    contact_email = scrapy.Field()
    ticket_info = scrapy.Field()
    # ticket_info = [
    #     {"ticket_price": "",
    #      "ticket_name": "",
    #      "ticket_description": ""},
    #     {"ticket_price": "",
    #       "ticket_name": "",
    #       "ticket_description": ""}]
    speaker_info = scrapy.Field()
    # speaker_info = [
    #     {
    #         "first_name": "",
    #         "last_name": "",
    #         "job_title": "",
    #         "affiliation": "",
    #         "bio": "",
    #         "photo": "",
    #         "url": "",
    #         "twitter": ""},
    #     {
    #         "first_name": "",
    #         "last_name": "",
    #         "job_title": "",
    #         "affiliation": "",
    #         "bio": "",
    #         "photo": "",
    #         "url": "",
    #         "twitter": ""}
    # ]

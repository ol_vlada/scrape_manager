from logging import root as logger
import scrapy
import logging
from datetime import datetime
from abc import abstractmethod
from scrapy import signals
from scrapy.http import Request
from scrapy_redis.spiders import RedisSpider
from scrape_app.scrape_app.items import ScrapeAppItem
from scrape_app.scrape_app.pipelines import ScrapeAppPipeline
from config import LOG_FILE_PATH

from utils.firebase_conf import FierebaseAPI
from scrapy.utils.log import configure_logging

from scrape_app.scrape_app.settings import FIREBASE_ENABLE
from titlecase import titlecase


configure_logging(install_root_handler=False)
logging.basicConfig(
    format='%(levelname)s: %(message)s',
    level=logging.INFO
)


class BaseSpider(RedisSpider):

    exclude_event_list = []
    start_spider = False

    def __init__(self, *args, **kwargs):
        super(BaseSpider, self).__init__(*args, **kwargs)
        self.firebase_instance = FierebaseAPI()
        self.pipeline = ScrapeAppPipeline()
        LOG_FILE = "%s/scrapy_%s_%s.log" % (LOG_FILE_PATH, self.name, datetime.now())
        logger.addHandler(logging.FileHandler(LOG_FILE))

    @staticmethod
    @abstractmethod
    def get_start_data(response):
        """
        Construct start data.
        :param response:
        :return: Should return dict with start_url and headers if needed
        For example:
        {
            url: 'http://example.com',
            headers: {'user_agent': 'ua_example'}
        }
        """
        raise NotImplementedError('get_start_data must be implemented in inherited class')


    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(BaseSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_error, signal=signals.spider_error)
        crawler.signals.connect(spider.spider_idle, signal=signals.spider_idle)
        return spider

    def spider_idle(self):
        """
         Schedules a request if available,
         otherwise called pipeline method for save data and waits
        """
        if FIREBASE_ENABLE and self.start_spider:
            self.pipeline.push_scrape_data(self.name)
            self.start_spider = False
        super(BaseSpider, self).spider_idle()

    def spider_error(self, *args, **kwargs):
        data = dict(error_data=kwargs['failure'].value)
        if FIREBASE_ENABLE:
            self.firebase_instance.add_result(self.name, data, 'failed')
            self.firebase_instance.update_status(self.name, 'idle')

    def make_request_from_data(self, data):
        """
        This method will be called when we receive a request.
        """
        start_data = self.get_start_data()
        if start_data.get('url'):
            if start_data.get('headers'):
                try:
                    if FIREBASE_ENABLE:
                        self.firebase_instance.update_status(self.name, 'panding')
                        logging.info('update spider status to panding')
                    return Request(start_data.get('url'), headers=start_data.get('headers'),
                                   dont_filter=True)
                except Exception as e:
                    self.logger.error(e)
            else:
                try:
                    if FIREBASE_ENABLE:
                        self.firebase_instance.update_status(self.name, 'panding')
                        logging.info('update spider status to panding')
                    return Request(start_data.get('url'), dont_filter=True)
                except Exception as e:
                    self.logger.error(e)
        raise Exception('Spider does not have start_url')

    @staticmethod
    def parse_next_page_link(links, callback):
        """
        Extract the link to the next page of products in a site.
        :param links: A links that will be used to extract the next page link.
        :param callback: A callback function that will be applied to the responses generated from the requests yielded
        from this function.
        :return: A Request object for the next page link.
        """
        try:
            for link in links:
                logger.info('requesting next page')
                yield scrapy.Request(
                    url=link,
                    callback=callback, dont_filter=True)
        except:
            raise TypeError('links type must be list')

    def parse_events_page(self, response):
        """
        Construct  items from a event page response object.
        :param response: A Response object from which the items will be generated.
        :return: A generator items for the events given by the response object.
        """
        event_type = self.get_event_type(response)
        if self.exclude_event_list and event_type:
            if isinstance(event_type, list):
                for event in event_type:
                    if event in self.exclude_event_list:
                        return
            else:
                raise TypeError('event type must be list')
        yield self.construct_item(response)

    def construct_item(self, response):
        """
        Construct a  item from the response object.
        :param response: A Response object from which the product item will be generated.
        """
        self.start_spider = True
        product = ScrapeAppItem()
        title = self.get_title(response)
        product['title'] = titlecase(title) if title else None
        product['brief_description'] = self.get_brief_description(response)
        product['description'] = self.get_description(response)
        product['date_from'] = self.get_date_from(response)
        product['date_to'] = self.get_date_to(response)
        product['start_time'] = self.get_start_time(response)
        product['end_time'] = self.get_end_time(response)
        product['event_type'] = ','.join(self.get_event_type(response)) if self.get_event_type(response) else None
        product['event_website'] = self.get_event_website(response)
        product['ticket_url'] = self.get_ticket_url(response)
        product['speakers'] = self.get_speakers(response)
        product['in_group_id'] = self.get_in_group_id(response)
        product['organization_name'] = self.get_organization_name(response)
        product['photo_url'] = self.get_photo_url(response)
        product['location'] = self.get_location(response)
        product['room'] = self.get_room(response)
        product['address'] = self.get_address(response)
        product['city'] = self.get_city(response)
        product['state'] = self.get_state(response)
        product['zip'] = self.get_zip(response)
        product['keywords'] = ','.join(self.get_keywords(response)) if self.get_keywords(response) else None
        product['hashtag'] = ','.join(self.get_hashtag(response)) if self.get_hashtag(response) else None
        product['contact_name'] = self.get_contact_name(response)
        product['contact_phone'] = self.get_contact_phone(response)
        product['contact_email'] = self.get_contact_email(response)
        product['ticket_info'] = self.get_ticket_info(response)
        product['speaker_info'] = self.get_speaker_info(response)
        return product

    @staticmethod
    @abstractmethod
    def get_title(response):
        raise NotImplementedError('get_title must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_brief_description(response):
        raise NotImplementedError('get_brief_description must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_description(response):
        raise NotImplementedError('get_description must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_date_from(response):
        """
        Construct date from
        :param response:
        :return: should be in MM/DD/YYYY format
        """
        raise NotImplementedError('get_date_from must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_date_to(response):
        """
        Construct date to
        :param response:
        :return: should be in MM/DD/YYYY format
        """
        raise NotImplementedError('get_date_to must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_start_time(response):
        """
        Construct start time
        :param response:
        :return: should be in HH:MM AM/PM format
        """
        raise NotImplementedError('get_start_time must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_end_time(response):
        """
        Construct end time
        :param response:
        :return: should be in HH:MM AM/PM format
        """
        raise NotImplementedError('get_end_time must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_event_type(response):
        """
        Construct event type
        :param response:
        :return: should be list of event type
        """
        raise NotImplementedError('get_event_type must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_event_website(response):
        """
        Construct event website url
        :param response:
        :return: should be URL of event page
        """
        raise NotImplementedError('get_event_website must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_ticket_url(response):
        """
        Same as event website for instances where there isn’t a separate Register/RSVP link
        """
        raise NotImplementedError('get_ticket_url must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_speakers(response):
        raise NotImplementedError('get_speakers must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_in_group_id(response):
        """
        Spider id
        :param response:
        :return: get from spreadsheet
        """
        raise NotImplementedError('get_in_group_id must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_organization_name(response):
        raise NotImplementedError('get_organization_name must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_photo_url(response):
        raise NotImplementedError('get_photo_url must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_location(response):
        """
        Name of place event is taking place, not including address (eg. Butler Library)
        """
        raise NotImplementedError('get_location must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_room(response):
        """
        Specific place of event (eg. 523)
        """
        raise NotImplementedError('get_room must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_address(response):
        """
        Full street of location, not including name (eg. 535 W. 114 St)
        """
        raise NotImplementedError('get_address must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_city(response):
        raise NotImplementedError('get_city must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_state(response):
        raise NotImplementedError('get_state must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_zip(response):
        raise NotImplementedError('get_zip must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_keywords(response):
        """
        Construct keywords
        :param response:
        :return: should be comma separated keywords associated with event
        """
        raise NotImplementedError('get_keywords must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_hashtag(response):
        """
        Only include if there is a structured hashtag (“#”) associated with event
        :param response:
        :return: should be comma separated hashtag associated with event
        """
        raise NotImplementedError('get_hashtag must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_contact_name(response):
        raise NotImplementedError('get_contact_name must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_contact_phone(response):
        raise NotImplementedError('get_contact_phone must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_contact_email(response):
        raise NotImplementedError('get_contact_email must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_ticket_info(response):
        """
           Construct ticket info.
           :param response:
           :return: ticket info should be in following format:
            ticket_info = [
                {"ticket_price": "",
                 "ticket_name": "",
                 "ticket_description": ""},
                {"ticket_price": "",
                  "ticket_name": "",
                  "ticket_description": ""}
                ]
        """
        raise NotImplementedError('get_ticket_info must be implemented in inherited class')

    @staticmethod
    @abstractmethod
    def get_speaker_info(response):
        """
            Construct  speaker info.
            :param response:
            :return: speaker info should be in following format:
            speaker_info = [
            {
                "first_name": "",
                "last_name": "",
                "job_title": "",
                "affiliation": "",
                "bio": "",
                "photo": "",
                "url": "",
                "twitter": ""},
            {
                "first_name": "",
                "last_name": "",
                "job_title": "",
                "affiliation": "",
                "bio": "",
                "photo": "",
                "url": "",
                "twitter": ""}
            ]
        """
        raise NotImplementedError('get_speaker_info must be implemented in inherited class')

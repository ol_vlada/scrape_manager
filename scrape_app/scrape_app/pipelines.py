# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
from utils.connect_to_redis import redis_connect
from utils.firebase_conf import FierebaseAPI
import config


class ScrapeAppPipeline(object):

    def __init__(self):
        self.r = redis_connect()
        self.firebase_instance = FierebaseAPI()

    def process_item(self, item, spider):
        self.r.lpush(spider.name, item)
        return item

    def close_spider(self, spider):
        data = self.r.lrange(spider.name, 0, -1)
        if data:
            self.firebase_instance.add_result(spider.name,
                                              data,
                                              config.parsing_status.get('success'))
            self.firebase_instance.update_status(spider.name,
                                             config.spider_status.get('idle'))

    def push_scrape_data(self, spider_name):
        data = self.r.lrange(spider_name, 0, -1)
        if data:
            self.firebase_instance.add_result(spider_name,
                                              data,
                                              config.parsing_status.get('success'))
            self.firebase_instance.update_status(spider_name,
                                                 config.spider_status.get('idle'))
            self.r.delete(spider_name)

# Scrapy manager

###**Installation instructions**

1. Clone the project
2. Create a virtual environment with python 3
3. Install development dependencies
    >pip install -r requirements.txt
    
### **Project description**

Project structure:
1. **app** ->  Flask app, which pushes start spider task to redis queue
2. **scrape_app** -> Scrapy app, which has the base spider with general methods and format instructions.
3. **utils** -> Here we have firebase Api, for cooperation with Firestore.
 And there will be other utility methods.
 
### **Development instructions**

1. Spider name has to match part of the domain. For example:
for site "https://events.columbia.edu" spider name has to be called "columbia" 
2. For each new spider, you have to create a branch called by the scraper.
3.  After spider is finished you have to submit Pull Request into develop branch. 
And add me to reviewers.
After a successful review, this spider will be merged. 
If not - will be returned for further fixes.

### **Spider instruction**

1. Base spider contains all instructions for creating new spiders. Base spider is inherited from RedisSpider. 
This is necessary so that the spiders are always in a state of waiting. 
For correct work *scrapy-redis* spider should not have start_urls and start_request method.
You need to implemented method **get_start_data** to specify start url and headers for the first request.

2. To **exclude some event type** you can use **exclude_event_list**. 
Add attribute in your class, with events list which necessary to exclude. For example: 
    >exclude_event_list = ['Exhibitions and Openings']
    
3. Don't change **base_spider.py** 
All changes should be agreed first. After any changes, all spiders must be verified.

4. For development, you can not use Scrapy-Redis. 
But after finishing the work on spider you need to change it on scrapy-redis and check functionality.

5. To use scrapy-redis:
    **run the spider:**
    >scrapy runspider myspider.py
    
    **push urls to redis:**
    >redis-cli lpush <spider_name>:start_urls <start_url>
    
    where, spider_name - name of your spider
    start_url - url for first request
    
6. For development, you can disable interaction of  spider and firebase. 
Set FIREBASE_ENABLE in settings to False. 

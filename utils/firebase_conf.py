from google.cloud import firestore
from config import firebase_settings_file, project_name
import os

from datetime import datetime, timezone


class FierebaseAPI(object):

    def __init__(self):
        os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = firebase_settings_file
        self.client = firestore.Client(project=project_name)
        self.collection_name = 'scrapers'

    def update_status(self, spider_name, new_status):
        spider_id = self.get_spider_id(spider_name)
        self.client.collection(self.collection_name).document(spider_id).update({'status': new_status})

    def add_result(self, spider_name, result_data, parsing_status):
        spider_id = self.get_spider_id(spider_name)
        try:
            data = [x.decode('utf-8') for x in result_data]
        except Exception as e:
            data = result_data
        spider_collection = self.client.collection(self.collection_name).document(spider_id)
        spider_collection.collection('result').add(dict(date=datetime.now(timezone.utc),
                                                        data=data,
                                                        parsing_status=parsing_status))

    def get_spider_id(self, spider_name):
        collection = self.client.collection(self.collection_name)
        all_collection = collection.get()
        for item in all_collection:
            data = item.to_dict()
            if 'name' in data and data['name'] == spider_name:
                id_collection = item.id
                return id_collection
        raise Exception('Spider with name - {} does not exist in db'.format(spider_name))


if __name__ == '__main__':
    f = FierebaseAPI()
    f.add_result('columbia', {'title':'sdfasdf', 'date_from':'12/12/12'}, 'success')

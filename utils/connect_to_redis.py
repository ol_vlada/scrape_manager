import redis
import config


def redis_connect():
    r = redis.Redis(host=config.REDIS_HOST,
                    port=config.REDIS_PORT)
    return r